#ifndef __UTIL_LIBCAMERA_H__
#define __UTIL_LIBCAMERA_H__

typedef struct _capture_dev_t capture_dev_t;
typedef struct _capture_frame_t capture_frame_t;

#ifdef __cplusplus
extern "C" {
#endif

int libcamera_start_capture(capture_dev_t *cap_dev);
void libcamera_get_capture_format(capture_dev_t *cap_dev, int *cap_w, int *cap_h,
                                  unsigned int *cap_fmt);
capture_dev_t *libcamera_init_camera(long unsigned int camera_index, int width, int height);
capture_frame_t *libcamera_acquire_capture_frame(capture_dev_t *cap_dev);
void libcamera_release_capture_frame(capture_dev_t *cap_dev, capture_frame_t *cap_frame);

#ifdef __cplusplus
}
#endif

#endif