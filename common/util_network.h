#ifndef _NETWORK_H_
#define _NETWORK_H_

#ifdef __cplusplus
extern "C" {
#endif

#include <sys/types.h>
#include <sys/socket.h>
#include <netdb.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <errno.h>
#include <pthread.h>

struct Data
{
    face_detect_result_t face_detect_ret;
    face_landmark_result_t face_mesh_ret[MAX_FACE_NUM];
    int draw_x, draw_y, draw_w, draw_h;
    int cur_texid_mask;
};

struct addrinfo hints;
struct addrinfo *result, *rp;
int sfd = -1, s = 0;
ssize_t nread = 0;
struct sockaddr_storage peer_addr;
socklen_t peer_addr_len;

void do_setup_client(char *hostname)
{
	memset(&hints, 0, sizeof(struct addrinfo));
	hints.ai_family = AF_UNSPEC;
	hints.ai_socktype = SOCK_DGRAM;
	hints.ai_flags = 0;
	hints.ai_protocol = 0;

	s = getaddrinfo(hostname, "1919", &hints, &result);
	if (s != 0)
	{
	    fprintf(stderr, "Could not resolve hostname: %s : %s\n", hostname, gai_strerror(s));
	    return;
	}

	for (rp = result; rp != NULL; rp = rp->ai_next)
	{
            char host[128] = { 0, };
            char port[8] = { 0, };
            getnameinfo(rp->ai_addr, rp->ai_addrlen,
                host, 128,
                port, 8,
                NI_NUMERICHOST | NI_NUMERICSERV);

            fprintf(stderr, "Connecting to: %s %s\n", host, port);

	    sfd = socket(rp->ai_family, rp->ai_socktype, rp->ai_protocol);
	    if (sfd == -1)
		continue;

	    if (connect(sfd, rp->ai_addr, rp->ai_addrlen) != -1) {
                fprintf(stderr, "Connected\n");
		break;
            }
            fprintf(stderr, "Failed to connect\n");

	    close(sfd);
	}

	if (rp == NULL)
	{
	    fprintf(stderr, "Could not connect\n");
	    exit(EXIT_FAILURE);
	}

	freeaddrinfo(result);
}

static void
setup_client_thread(char *hostname) {
  while (sfd < 0) {
    do_setup_client(hostname);
    sleep (2);
  }
}


void setup_client(char *hostname)
{
   static pthread_t thread;
  pthread_create (&thread, NULL, setup_client_thread, hostname);
}

int
setup_v4_server() {
  int fd = -1;
  struct sockaddr_in v4_any = {0};
  v4_any.sin_port = htons(1919);

  fd = socket(AF_INET, SOCK_DGRAM, 0);
  if (fd == -1)
  {
    fprintf(stderr, "Can't setup ipv4 socket: %s\n", strerror(errno));
    return -1;
  }

  if (bind(fd, (struct sockaddr *)&v4_any, sizeof(v4_any)) < 0) {
    fprintf(stderr, "Can't bind ipv4 socket: %s\n", strerror(errno));
    return -1;
  }

  return fd;
}

int
setup_v6_server() {
  int fd = -1;
  struct sockaddr_in6 v6_any = {0};
  v6_any.sin6_family = AF_INET6;
  v6_any.sin6_port = htons(1919);
  int zero = 0;

  fd = socket(AF_INET6, SOCK_DGRAM , 0);
  if (fd == -1) {
    fprintf(stderr, "Can't setup ipv6 socket: %s\n", strerror(errno));
    return -1;
  }

  /* Explicitly ask for this socket to do both v4 and v6 even though it's the default in linux */
  setsockopt(fd, IPPROTO_IPV6, IPV6_V6ONLY, &zero, sizeof(zero));

  if (bind(fd, (struct sockaddr *)&v6_any, sizeof(v6_any)) < 0) {
    fprintf(stderr, "Can't bind ipv6 socket: %s\n", strerror(errno));
    return -1;
  }

  return fd;
}

void setup_server()
{

  printf("Setting up server... Binding on v6 first\n");
  sfd = setup_v6_server();

  if (sfd <= 0) {
     printf("ipv6 failed, falling back to v4\n");
     sfd = setup_v4_server();
  }

  if (sfd < 0) {
    printf("Failed to setup server... \n");
    exit(1);
  }

  printf("Listening!\n");
}

int server_read(struct Data* data_render)
{
    peer_addr_len = sizeof(struct sockaddr_storage);
    return recvfrom(sfd, data_render, sizeof(*data_render), 0, (struct sockaddr *) &peer_addr, &peer_addr_len);
}

#ifdef __cplusplus
}
#endif
#endif
